COMPONENT=hydra
APP_NAME=hydra
NAMESPACE=auth
TAG=latest

.PHONY: install-deps
install-deps:
	go get -v -u github.com/Masterminds/glide
	go get -v -u github.com/githubnemo/CompileDaemon
	go get -v -u gopkg.in/alecthomas/gometalinter.v1
	go get -v -u github.com/jstemmer/go-junit-report
	glide install

.PHONY: build
build:
	-rm -rf .bin
	go build -o .bin/$(APP_NAME) -installsuffix cgo .
	chmod +x .bin/$(APP_NAME)

.PHONY: cross
cross:
	-rm -rf .bin
	CGO_ENABLED=0 GOOS=linux go build -o .bin/$(APP_NAME) -a -installsuffix cgo .
	chmod +x .bin/$(APP_NAME)

.PHONY: demo
demo:
	SYSTEM_SECRET=supers3cr3t! DOCKER_IP=localhost docker-compose -f docker-compose-demo.yml up --build

.PHONY: clean
clean:
	docker-compose down
	docker rm $(shell docker ps -aq)
	docker volume rm $(shell docker volume ls)

.PHONY: test
test:
	go test -v $$(glide nv)
	
.PHONY: deploy-image
deploy-image:
	docker build -t ${REGISTRY_URL}/${COMPONENT}:${TAG} .
	docker push ${REGISTRY_URL}/${COMPONENT}:${TAG}

.PHONY: helm-start
helm-start:
	helm install ./deployment/charts/hydra --namespace=${NAMESPACE} --name=${APP_NAME} 

.PHONY: helm-stop
helm-stop:
	@echo "Helm stop not implemented"

.PHONY: helm-upgrade
helm-upgrade:
	helm upgrade --namespace ${NAMESPACE} ./deployment/charts/hydra 

