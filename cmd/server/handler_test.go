package server

import (
	"testing"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/koral-io/hydra/config"
)

func TestStart(t *testing.T) {
	router := httprouter.New()
	h := &Handler{
		Config: &config.Config{
			DatabaseURL: "memory",
		},
	}
	h.registerRoutes(router)
}
