package config

import (
	"github.com/ory/fosite"
	"github.com/ory/fosite/handler/oauth2"
	"gitlab.com/koral-io/hydra/firewall"
	"gitlab.com/koral-io/hydra/jwk"
	"gitlab.com/koral-io/hydra/pkg"
	"gitlab.com/koral-io/hydra/warden/group"
	"github.com/ory/ladon"
)

type Context struct {
	Connection interface{}

	Hasher         fosite.Hasher
	Warden         firewall.Firewall
	LadonManager   ladon.Manager
	FositeStrategy oauth2.CoreStrategy
	FositeStore    pkg.FositeStorer
	KeyManager     jwk.Manager
	GroupManager   group.Manager
}
