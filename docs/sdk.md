# SDK

An SDK is available for Golang via `go get gitlab.com/koral-io/hydra/sdk`. We are planning on implementing SDKs for other languages too.
